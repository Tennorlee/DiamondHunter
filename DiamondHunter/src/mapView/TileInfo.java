/**
 * this is the class consisting information of a specific tile. 
 */

package mapView;

public class TileInfo {
	
	private int col;	//coordinates
	private int row;
	private int tileType;
	private int entityType;
	private boolean isNormal;
	private boolean isEntity;
	
	//for the tileType which is the index of the image getting from .gif file
	public static final int FIELD = 1;	// walkable tile image
	public static final int GRASS = 2;	// numbers assigned to each item to distinguish between each other
	public static final int FLOWER = 3; // as their numbers gotten from .gif file are different
	
	// for set entity
	public static final int NOTENTITY = -1;  //entity types
	public static final int BOAT = 0;
	public static final int AXE = 1;
	public static final int DIAMOND = 2;
	public static final int PLAYER = 3;
		
	/**
	 * @param column , row , tile types
	 */
	public TileInfo(int col, int row, int tileType) {
		this.col=col;
		this.row=row;
		this.tileType = tileType;
		this.entityType = NOTENTITY;
		this.isEntity = false;
		setIfNormal();
	}
	
	private void setIfNormal() {
		//if conditions are true then isnormal=true,else isnormal=false
		this.isNormal = (tileType == GRASS || tileType == FIELD || tileType == FLOWER) ? true : false;   
	}
	
	private void setEntity(boolean isEntity) {
		this.isEntity = isEntity;
	}
	
	public void setEntityType(int entityType) {
		this.entityType = entityType;
		setEntity((this.entityType != -1) ? true : false);	// set if it's not entity then 0 else 1
	}
	
	public boolean isEntity() {
		return isEntity;
	}
	
	public boolean isNormal() {
		return isNormal;
	}
	
	public int getCol() {
		return col;
	}
	public int getRow() {
		return row;
	}
	
}
