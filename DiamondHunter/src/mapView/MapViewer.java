/**
 * This is the class where the application is set up and everything works here.
 */
package mapView;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * @author User
 */
public class MapViewer extends Application {

	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 * @param primary stage
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			Parent root = FXMLLoader.load(getClass().getResource("MapViewerScene.fxml"));
			Scene scene = new Scene(root,800,800);
		
			primaryStage.setTitle("Map Viewer");	//naming window title
			primaryStage.getIcons().add(new Image("Icon/mapIcon.png"));		//setting window icon
			primaryStage.setScene(scene);	//setting new scene on the stage
			primaryStage.setResizable(false);	//secured/fixed window size
			primaryStage.show();
		} 
		catch(Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * launching application
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
