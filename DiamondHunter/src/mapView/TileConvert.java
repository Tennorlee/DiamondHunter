/**
 * TileConvert is used as a converter of buffered image to writable image
 * what can be used in javafx.
 */

package mapView;

import java.awt.image.BufferedImage;

import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

/**
 * @author User
 *	Retrieved from: https://www.tutorialspoint.com/javafx/javafx_images.htm
 *	As javafx uses different imageType to normal java files, pixelwriter and 
 *	pixelreader is used instead.
 */
public class TileConvert {
	BufferedImage buffImg;
	
	public TileConvert(BufferedImage bfimg) {
		this.buffImg=bfimg;
	}
	
	public WritableImage getWrittenImg(){
		//Creating an image 
	      int width = (int)buffImg.getWidth(); 
	      int height = (int)buffImg.getHeight(); 
	         
	      //Creating a writable image 
	      WritableImage wImage = new WritableImage(width, height); 
	         
	      //getting the pixel writer 
	      PixelWriter writer = wImage.getPixelWriter();           
	      
	      //Reading the color of the image 
	      for(int y = 0; y < height; y++) { 
	         for(int x = 0; x < width; x++) { 
	            //Retrieving the color of the pixel of the loaded image   
	            int color = buffImg.getRGB(x, y); 
	              
	            //Setting the color to the writable image 
	            writer.setArgb(x, y, color);              
	         }
	      }
		return wImage;
	}
}
