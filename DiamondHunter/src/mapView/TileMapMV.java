/**
 * The tile map class contains a loaded tile set
 * and a 2D array of the map.
 * Each index in the map corresponds to a specific tile.
 */

package mapView;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;

public class TileMapMV {
	// map
	private int[][] map;
	private int tileSize;
	private int numRows;
	private int numCols;
	
	// tileset
	private BufferedImage tileset;
	private int numTilesAcross;
	private WritableImage[][] tiles;
		
	public static final int WIDTH = 640;
	public static final int HEIGHT = 640;

	/**
	 * tileMapMV is a clone of TileMap class in game with some 
	 * modification on drawing method
	 * @param tileSize=16
	 */
	public TileMapMV(int tileSize) {
		this.tileSize = tileSize;
	}
	
	/**
	 * Load tile graphics.
	 * graphic dimension: 320 * 32
	 * @param s
	 */
	public void loadTiles(String s) {
		
		try {

			tileset = ImageIO.read(
				getClass().getResourceAsStream(s)
			);
			
			// calculate the total number of tiles across
			numTilesAcross = tileset.getWidth() / tileSize;
			tiles = new WritableImage[2][numTilesAcross];
			
			BufferedImage subimage;
			for(int col = 0; col < numTilesAcross; col++) {
				//getSubimage(x of upperleft of image, y of upperleft of image, width, height)
				// read 1st row
				subimage = tileset.getSubimage(
							col * tileSize,
							0,
							tileSize,
							tileSize
						);
				tiles[0][col] = new TileConvert(subimage).getWrittenImg();
				//read 2nd row
				subimage = tileset.getSubimage(
							col * tileSize,
							tileSize,
							tileSize,
							tileSize
						);
				tiles[1][col] = new TileConvert(subimage).getWrittenImg();
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * @param s loading the tileMap.map .MAP filetype
	 */
	public void loadMap(String s) {
		
		try {
			
			InputStream in = getClass().getResourceAsStream(s);
			BufferedReader br = new BufferedReader(
						new InputStreamReader(in)
					);
			
			numCols = Integer.parseInt(br.readLine());
			numRows = Integer.parseInt(br.readLine());
			map = new int[numRows][numCols];

			// "\s"captures whitespace while "+" makes it captures at least once
			String delims = "\\s+";
			for(int row = 0; row < numRows; row++) {
				String line = br.readLine();
				String[] tokens = line.split(delims);
				for(int col = 0; col < numCols; col++) {
					map[row][col] = Integer.parseInt(tokens[col]);
				}
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}	
	}
		
	/**
	 * by reading map[][], it takes the image from .gif file to 
	 * draw the image on canvas referencer
	 */
	public void drawMap(GraphicsContext g) {
		for(int row = 0; row < numRows; row++) {
			for(int col = 0; col < numCols; col++) {
				
				int rc = map[row][col];
				int r = rc / numTilesAcross;
				int c = rc % numTilesAcross;

				g.save();
				g.drawImage(
					tiles[r][c],
					col * tileSize,
					row * tileSize
				);
			}
		}
	}


	public int getNumCols() {
		return numCols;
	}

	public int getNumRows() {
		return numRows;
	}

	public int getTileSize() {
		return 16;
	}

	public int getMap(int row, int col) {
		return map[row][col];
	}	
}



















