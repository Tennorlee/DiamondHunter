package mapView;

import java.net.URL;
import java.util.ResourceBundle;

import com.neet.DiamondHunter.Main.Game;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;

public class Controller implements Initializable{
	
	private int[] initAxePos;	
	private int[] initBoatPos;
	private int[] playerPos;
	private int[][] diamondPos;
	private static int[] savedAxePos;
	private static int[] savedBoatPos;
	
	private boolean canSetAxe= false;
	private boolean canSetBoat=false;
	private boolean isGameLaunched = false; 	//when mapviewer is opened, game is not opened
	public static boolean isSaved = false;		//saving axe and boat positions
	private GraphicsContext mapGraphics;		
	private TileMapMV tileMap;
	private TileInfo[][] maptileInfo; 	//info of each tile loaded
	
	@FXML	private Canvas mapCanvas;
	@FXML 	public ImageView mapImageView;
	@FXML	private GridPane mapGrid;
	@FXML	private StackPane mapStack;
	@FXML	private AnchorPane mapPane;
	@FXML	private Label axePos;
	@FXML	private Label cursorPos;
	@FXML	private Label boatPos;
	@FXML	private Label instruction;
	@FXML	private Label warning;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			int tileSize=16;
			initAxePos = new int[]{26,37}; 	// show the coordinates of axe (row,col)
			initBoatPos =  new int[]{12,4}; // show the coordinates of boat (row,col)
			playerPos = new int[]{17,17}; 	// show the initial coordinates of the player (row,col)
			
			diamondPos = new int[][]{		// diamond position (row,col)
				{20,20}, {12,36}, {28,4}, {4,34}, {28,19}, {35,26}, {38,36}, {27,28}, 
				{20,30}, {14,25}, {4,21}, {9,14}, {4,3}, {20,14}, {13,20}
				};
		
			tileMap = new TileMapMV(tileSize);	//try to draw map
			
			canvasInit();	//trigger canvas initiallizer  
	        initLabel();	//trigger label initiallizer 
			
			mapStack.setPrefSize((double)(tileMap.getNumRows()*tileMap.getTileSize()) , (double)(tileMap.getNumCols()*tileMap.getTileSize()));
			mapPane.setMinSize(mapStack.getPrefWidth(),mapStack.getPrefHeight());
			
			gridInit();		//trigger grid initializer
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * initializes the canvas by 
	 */
	private void canvasInit() {
		
		mapCanvas.setWidth((double) TileMapMV.WIDTH);	// initialize the size of canvas
		mapCanvas.setHeight((double) TileMapMV.HEIGHT);

		tileMap.loadMap("/Maps/testmap.map");	// load files
		tileMap.loadTiles("/Tilesets/testtileset.gif");
		
		mapGraphics = mapCanvas.getGraphicsContext2D();		// set reference to draw on canvas
		tileMap.drawMap(mapGraphics);
		mapCanvas.getGraphicsContext2D().drawImage(mapGraphics.getCanvas().snapshot(new SnapshotParameters(), null), 0, 0);
	}
	
	/**
	 * initializes the grids
	 */
	private void gridInit() {
		maptileInfo = new TileInfo[tileMap.getNumRows()][tileMap.getNumCols()]; 	//initialize a total of 40*40 TileInfo array
		for(int i=0 ; i < maptileInfo.length ; i++) { 	//for every maptileInfo
			mapGrid.getColumnConstraints().add(new ColumnConstraints((double)(tileMap.getTileSize()))); 	// fix grid width
			mapGrid.getRowConstraints().add(new RowConstraints((double)(tileMap.getTileSize()))); 	// fix grid height
		}
		for(int row=0 ; row < tileMap.getNumRows() ; row++) {	
			for(int col=0 ; col < tileMap.getNumCols() ; col++) {
				maptileInfo[row][col] = new TileInfo( row , col , tileMap.getMap(row,col) );
				addTile(col, row);		
			}
		}
	}

	/**
	 * adding tiles
	 * giving them eventhandler like if hover over
	 * and if clicked.
	 */
	private void addTile(int col, int row) {
		String StoreCoord;
        Label label = new Label();
        label.setMinSize(tileMap.getTileSize(), tileMap.getTileSize());
        StoreCoord = Integer.toString(row)+ "," + Integer.toString(col);

		showAllSpriteOnGrid(label, col, row);

        label.setUserData(maptileInfo[row][col]);

        label.setOnMouseEntered(e -> {
            cursorPos.setText(StoreCoord);
        });
        
        label.setOnMouseClicked(e -> {
        	if(canSetAxe||canSetBoat) {
        		if(maptileInfo[row][col].isNormal()&& !maptileInfo[row][col].isEntity()) {
        			if(canSetAxe) {
                		axePos.setText(StoreCoord);
                		canSetAxe=false;
                		initAxePos[0]=row;
                		initAxePos[1]=col;
                	}
                	if(canSetBoat) {
                		boatPos.setText(StoreCoord);
                		canSetBoat=false;
                		initBoatPos[0]=row;
                		initBoatPos[1]=col;
                	}
                	warning.setText("");
                	refresh();
        		}
        		else {
        			warning.setText("WARNING:\n\nYou can't put the item here!");
        		}
        	}
        });

        mapGrid.add(label, col, row);

	}
	
	/**
	 * populate the sprite on positions
	 * @param label , column , row
	 */
	private void showAllSpriteOnGrid(Label label,int col,int row) {
			// if is axePos
		if(comparePos(row,col,initAxePos)) {
			label.setGraphic(new ImageView(new SpriteLoader().getAxeImg()));
			maptileInfo[row][col].setEntityType(TileInfo.AXE);
		}	//if is boatPos
		else if(comparePos(row,col,initBoatPos)) {
			label.setGraphic(new ImageView(new SpriteLoader().getBoatImg()));
			maptileInfo[row][col].setEntityType(TileInfo.BOAT);
		}	//if is playerPos
		else if(comparePos(row,col,playerPos)) {
			label.setGraphic(new ImageView(new SpriteLoader().getPlayerImg()));
			maptileInfo[row][col].setEntityType(TileInfo.PLAYER);
		}	//if is diamondsPos
		else if(checkIfDiamond(row,col)) {
			label.setGraphic(new ImageView(new SpriteLoader().getDiamondImg()));
			maptileInfo[row][col].setEntityType(TileInfo.DIAMOND);
		}
		
	}
	
	private void refresh() {
		mapGrid.getChildren().clear();
		
		maptileInfo = new TileInfo[tileMap.getNumRows()][tileMap.getNumCols()]; 	//initialize a total of 40*40 TileInfo array
		
		for(int row=0 ; row < tileMap.getNumRows() ; row++) {
			for(int col=0 ; col < tileMap.getNumCols() ; col++) {
				maptileInfo[row][col] = new TileInfo( row , col , tileMap.getMap(row,col) );
				addTile(col, row);
			}
		}
		
	}
	
	
	/**
	 * check through 15 diamond positions
	 * @param r row
	 * @param c column
	 * @return if equal then true else false
	 */
	private boolean checkIfDiamond(int r,int c) {
		for (int i=0;i<15;i++) {
			if(comparePos(r,c,diamondPos[i])) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * comparing position of current row.column to default positions
	 * @param r row
	 * @param c column
	 * @param s coordinates
	 * @return if equal then true else false
	 */
	private boolean comparePos(int r, int c, int[] s){
		return ( r==s[0] && c==s[1] )? true : false ;
	}
	
	/**
	 * initialize the labels
	 */
	private void initLabel() {
		cursorPos.setText("");
        axePos.setText("26,37");
        boatPos.setText("12,4");
        instruction.setText(" Instruction:\n\n\n 1. Press 'set' button to set position of items.\n\n" 	//initialize and display instructions 
        		+" 2. Press 'Save' button to save changes of the item location.\n\n"
        		+" 3. Press 'Reset' button to reset the items back to default locations.\n\n"
        		+" 4. Press 'Launch Game' to start the game.\n\n\n"
        		+"P/s: Please save the changes before starting the game!");
	}

	
	/**
	 * set trigger for setting new axe position
	 * @param event
	 */
	@FXML
	public void setAxePos(ActionEvent event) {		 
		if(!canSetAxe && !canSetBoat) {				// when axe set button is pressed but no changes made on axe position,
			canSetAxe = true;
			isSaved = false;
		}
		else if(canSetAxe){							// and user still presses axe set button once again, 
			warning.setText("WARNING:\n\n   It's ON already!");	//it display this warning text
		}
		else {										//otherwise display this if user presses boat set button
			warning.setText("WARNING:\n\n   Boat set is ON!");	    //it display this instead
		}
	}
	
	/**
	 * set trigger for setting new boat position
	 * @param event
	 */
	@FXML
	public void setBoatPos(ActionEvent event) {			
		if(!canSetAxe && !canSetBoat) {				// when boat set button is pressed but no changes made on boat position,
			canSetBoat = true;
			isSaved = false;
		}else if(canSetBoat){						// and user still presses boat set button once again, 
			warning.setText("WARNING:\n\n   It's ON already!");		
		}
		else {									    //otherwise display this if user presses boat set button
			warning.setText("WARNING:\n\n   Axe set is ON!");			//warning text displayed
		}
	}
	
	/**
	 * @return the savedAxePos
	 */
	public static int[] getSavedAxePos() {
		return savedAxePos;
	}

	/**
	 * @param savedAxePos the savedAxePos to set
	 */
	public void setSavedAxePos(int[] savedAxePos) {
		Controller.savedAxePos = savedAxePos;
	}
	
	/**
	 * @return the savedBoatPos
	 */
	public static int[] getSavedBoatPos() {
		return savedBoatPos;
	}

	/**
	 * @param savedBoatPos the savedBoatPos to set
	 */
	public void setSavedBoatPos(int[] savedBoatPos) {
		Controller.savedBoatPos = savedBoatPos;
	}
	
	/**
	 * reset axe and boat positions
	 * @param event
	 */
	@FXML
	public void reset(ActionEvent event) {
		initAxePos[0]=26;		// default axe and boat positions
		initAxePos[1]=37;
		initBoatPos[0]=12;
		initBoatPos[1]=4;
		System.out.println(initAxePos[0]+" "+initAxePos[1]);		// display default axe and boat positions
		System.out.println(initBoatPos[0]+" "+initBoatPos[1]);
		axePos.setText(initAxePos[0]+", "+initAxePos[1]);
		boatPos.setText(initBoatPos[0]+", "+initBoatPos[1]);
		refresh();		// re-read after the trigger executes
	}
	
	/**
	 * save changes of new positions
	 * @param event
	 */
	@FXML 
	public void saveChanges(ActionEvent event) {
		if(!isSaved) { 							// if the isSaved trigger is false
			setSavedAxePos(initAxePos);			// save axe and boat positions
			setSavedBoatPos(initBoatPos);
			isSaved=true; 						// open trigger
			warning.setText("Saved!");
		}
	}

	/**
	 * launches game 
	 * @param e
	 */
	@FXML
	public void launchGame(ActionEvent e) {
		if(!isGameLaunched) {					// if the isGameLaunched trigger is false
			Game.main(null);					// let main=null
			isGameLaunched=!isGameLaunched;		// when mapviewer opened, game not opened
		}else {
			warning.setText("WARNING:\n\n The Game has launched!");
		}
	}

	
}
