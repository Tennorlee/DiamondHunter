/**
 * This class gets the sprites of player, diamonds, axe and boat.
 */
package mapView;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import com.neet.DiamondHunter.Manager.Content;

import javafx.scene.image.WritableImage;

/**
 * @author User
 *
 */
public class SpriteLoader {
	//load sprites
	private WritableImage player = load("/Sprites/playersprites.gif", 16, 16, 0);
	private WritableImage diamond = load("/Sprites/diamond.gif", 16, 16, 0);
	private WritableImage[][] items = load("/Sprites/items.gif", 16, 16);
	private WritableImage axe = items[1][1];
	private WritableImage boat = items[1][0];
	
	public static WritableImage[][] load(String s, int w, int h) {
		WritableImage[][] ret;
		try {
			BufferedImage spritesheet = ImageIO.read(Content.class.getResourceAsStream(s));
			int width = spritesheet.getWidth() / w;
			int height = spritesheet.getHeight() / h;
			ret = new WritableImage[height][width];
			for(int i = 0; i < height; i++) {
				for(int j = 0; j < width; j++) {
					ret[i][j] = new TileConvert(spritesheet.getSubimage(j * w, i * h, w, h)).getWrittenImg();
				}
			}
			return ret;
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error loading graphics.");
			System.exit(0);
		}
		return null;
	}
	
	public static WritableImage load(String s, int w, int h, int n) {
		WritableImage ret;
		try {
			BufferedImage spritesheet = ImageIO.read(Content.class.getResourceAsStream(s));
			ret = new TileConvert(spritesheet.getSubimage(n, n, w, h)).getWrittenImg();
			
			return ret;
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error loading graphics.");
			System.exit(0);
		}
		return null;
	}
	
	public WritableImage getPlayerImg() {
		return player;
	}
	
	public WritableImage getDiamondImg() {
		return diamond;
	}
	
	public WritableImage getAxeImg() {
		return axe;
	}
	
	public WritableImage getBoatImg() {
		return boat;
	}

}
